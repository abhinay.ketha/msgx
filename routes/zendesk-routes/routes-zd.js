var zd_instance = require('../../bi/zendesk-bi/zendesk'),
    config_mandat = 'Username,Domain & Apikey';
function config_verification(payload) {
    if (payload.username && payload.username !== '' && payload.domain && payload.domain !== '' &&
        payload.apikey && payload.apikey !== '')
        return true;
    return false;
}
module.exports = function (app) {
    app.post('/api/zendesk/create_org', function (req, res) {
        if (config_verification(req.body) && req.body.name && req.body.name !== '') {
            zd_instance.initialise_module(req.body);
            zd_instance.create_org(req.body)
                .then((data) => res.send(data))
                .catch((err) => res.send(err));
        }
        else res.send('Organisation name,' + config_mandat + ' are mandatory');
    });

    app.post('/api/zendesk/create_ticket', function (req, res) {
        if (config_verification(req.body) && req.body.subject && req.body.subject !== '') {
            zd_instance.initialise_module(req.body);
            zd_instance.create_ticket(req.body)
                .then((data) => res.send(data))
                .catch((err) => res.send(err));
        }
        else res.send('Subject,' + config_mandat + ' are mandatory');
    });
    app.post('/api/zendesk/create_user', function (req, res) {
        if (config_verification(req.body) && req.body.name && req.body.name !== '' && req.body.email && req.body.email !== '') {
            zd_instance.initialise_module(req.body);
            zd_instance.create_user(req.body)
                .then((data) => res.send(data))
                .catch((err) => res.send(err));
        }
        else res.send('Name,Email ' + config_mandat + ' are mandatory');
    });
    app.post('/api/zendesk/list_users', function (req, res) {
        if (config_verification(req.body)) {
            zd_instance.initialise_module(req.body);
            zd_instance.list_users()
                .then((data) => res.send(data))
                .catch((err) => res.send(err));
        }
        else res.send(config_mandat);

    });
    app.post('/api/zendesk/find_user', function (req, res) {
        if (config_verification(req.body) && req.body.userid && req.body.userid !== '') {
            zd_instance.initialise_module(req.body);
            zd_instance.find_user(req.body.userid)
                .then((data) => res.send(data))
                .catch((err) => res.send(err));
        }
        else res.send('User Id is Mandatory');
    });
    app.get('/api/zendesk/find_ticket', function (req, res) {
        if (req.query.ticketid && req.query.ticketid !== '')
            zd_instance.find_ticket(req.body.ticketid)
                .then((data) => res.send(data))
                .catch((err) => res.send(err));
        else re.send('Ticket Id is mandatory');
    })
}