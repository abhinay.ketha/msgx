var fd_instance = require('../../bi/freshdesk-bi/freshdesk');

module.exports = function (app) {

    app.post('/api/freshdesk/create_ticket', function (req, res) {
        if (req.body.sub && req.body.sub !== '' && req.body.desc && req.body.desc !== ''
            && req.body.email && req.body.email !== '')
            fd_instance.createticket(req.body)
                .then((data) => res.send(data))
                .catch((err) => res.send(err));
        else res.send('Subject,Email and Description are mandatory');
    });
    app.post('/api/freshdesk/create_company', function (req, res) {
        if (req.body.name && req.body.name !== '')
            fd_instance.create_company(req.body)
                .then((data) => res.send(data))
                .catch((err) => res.send(err))
        else res.send('Company name is mandatory');
    });
    app.get('/api/freshdesk/forum_categories', function (req, res) {
        fd_instance.list_forum_categories()
            .then((data) => res.send(data))
            .catch((err) => res.send(err));
    });
    app.get('/api/freshdesk/forums_in_categorie', function (req, res) {
        if(req.query.catid && req.query.catid !== '')
        fd_instance.list_all_forums_in_categories(req.body.catid)
            .then((data) => res.send(data))
            .catch((err) => res.send(err));
        else res.send('category id is mandatory');
    });
    app.post('/api/freshdesk/forum_topic', function (req, res) {
        if (req.body.forumid && req.body.title && req.body.title !== ''
            && req.body.message && req.body.message !== '')
            fd_instance.create_forum_topic(req.body)
                .then((data) => res.send(data))
                .catch((err) => res.send(err));
        else res.send('ForumID,Title And Message are compulsory');
    });
}
