var dao_instance = require('../../dao/mongo-dao/mongo-dao-impl');
module.exports = function (app) {
    app.post('/api/near-by-loc/insert-agent', (req, res) => {
        dao_instance.fetch_dealer(req.body)
            .then((data) => {
                if (data.length > 0) {
                    var arr1 = data[0].address, arr2 = req.body.address;
                    var merged_arr = arr1.concat(arr2);
                    req.body.address = merged_arr
                }
                // console.log(req.body)
                dao_instance.insert_dealer_loc(req.body)
                    .then((data) => res.send(data))
                    .catch((err) => res.send(err))
            })
            .catch((err) => res.send(err))
    });

    app.get('/api/near-by-loc/list-agent', (req, res) => {
        dao_instance.fetch_dealer(req.query)
            .then((data) => res.send(data))
            .catch((err) => res.send(err))
    });

    app.get('/api/google/textsearch/', (req, res) => {
        dao_instance.googlemaps_textsearch(req.query)
            .then((data) => res.send(data))
            .catch((err) => res.send(err))
    });


    app.get('/api/google/nearbysearch/', (req, res) => {
        dao_instance.googlemaps_nearbysearch(req.query)
            .then((data) => res.send(data))
            .catch((err) => res.send(err))
    })
}