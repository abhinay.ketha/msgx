var sf_instance = require('../../bi/sforce-bi/sforce');
module.exports = function (app) {
    app.post('/api/sforce/create-lead', (req, res) => {
        if (req.body.lastname && req.body.lastname !== '' && req.body.lastname.length < 256 &&
            req.body.company && req.body.company !== '' && req.body.company.length < 256)
            sf_instance.create_lead(req.body)
                .then((data) => res.send(data))
                .catch((err) => res.send(err));
    });

    app.get('/api/sforce/industry-list', (req, res) => {
        res.send(sf_instance.list_industry());
    });

    app.post('/api/sforce/create-contact', (req, res) => {
        if (req.body.lastname && req.body.lastname !== '' && req.body.lastname.length < 256)
            sf_instance.create_contact(req.body)
                .then((data) => res.send(data))
                .catch((err) => res.send(err))
    });

    app.get('/api/sforce/fetch-account', (req, res) => {
        if (req.query.accid && req.query.accid !== '') {
            sf_instance.fetch_account_by_id(req.query)
                .then((data) => res.send(data))
                .catch((err) => res.send(err))
        }
        else res.send('Account id is mandatory');
    });

    app.get('/api/sforce/fetch-all-accounts', (req, res) => {
        sf_instance.fetch_all_accounts()
            .then((data) => res.send(data))
            .catch((err) => res.send(err))
    })
}