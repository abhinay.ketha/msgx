var mongoose = require('mongoose'),
    mongo_url = 'mongodb://localhost/msgx';
mongoose.connect(mongo_url, (err, res) => {
    if (err) return console.log(err)
    console.log('mongo connected')
});

exports.dealer_loc = mongoose.model('dealer_loc', {
    bot_id: String,
    pincode: String,
    address: Array
}, 'dealer_loc');