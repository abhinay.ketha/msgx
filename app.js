var express = require('express'),
    app = express(),
    cluster = require('cluster'),
    bodyparser = require('body-parser'),
    routes_fd = require('./routes/freshdesk-routes/routes-fd'),
    routes_zd = require('./routes/zendesk-routes/routes-zd'),
    routes_sf = require('./routes/sforce-routes/routes-sforce'),
    routes_near_by_loc = require('./routes/near-by-loc-routes/near-by-loc'),
    port = process.env.PORT || 3000;
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));
app.listen(port, function () {
    console.log('listening on port '+port);
});
routes_fd(app);
routes_zd(app);
routes_sf(app);
routes_near_by_loc(app);
app.get('/', (req, res) => {
    res.send({msg:'welcome to msgx from port 3000'});
});
