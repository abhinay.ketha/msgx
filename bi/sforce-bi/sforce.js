var sforce = require('jsforce'),
    appconfig = require('../../config/bi-config'),
    conn = new sforce.Connection();
conn.login(appconfig.sforce_abhinay.username, appconfig.sforce_abhinay.pwd, function (err, res) {
    if (err) return console.error(err);
    console.log(res)
});

module.exports = {
    create_lead: function (payload) {

        // var lead = {email: "abhinay.ketha@meltag.com",firstname: "abhinay",lastname: "req.body.last_name",title: "req.body.job_title",company: "req.body.company",country: "Ind",description: "description",industry: "industry",Website: "www.kuchbhi.com",MobilePhone: '9098765431',phone: "9032336311",PostalCode: "500074",Title: "Title",Status: "Status",IsUnreadByOwner: true,leadsource: 'Clearing Microsite'};
        var lead = {
            email: payload.email,
            firstname: payload.firstname,
            lastname: payload.lastname,
            title: payload.title,
            company: payload.company,
            country: payload.country,
            description: payload.desc,
            industry: payload.industry,
            Website: payload.website,
            MobilePhone: payload.mobile,
            phone: payload.phone,
            PostalCode: payload.postalcode,
            Title: payload.title,
            Status: payload.status,
            leadsource: payload.leadsource
        };
        return new Promise((resolve, reject) => {
            conn.sobject("Lead").create(lead, (err, ret) => {
                if (err || !ret.success) return reject(err);
                resolve(ret);
            });
        });
    },

    list_industry: function () {
        var industrylist = ['Agriculture', 'Apparel', 'Banking', 'Biotechnology', 'Chemicals', 'Communications', 'Construction', 'Consulting', 'Education', 'Electronics', 'Energy', 'Engineering', 'Entertainment', 'Environmental', 'Finance', 'Food & Beverage', 'Government', 'Healthcare', 'Hospitality', 'Insurance', 'Machinery', 'Manufacturing', 'Media', 'Not For Profit', 'Other', 'Recreation', 'Retail', 'Shipping',
            'Technology', 'Telecommunications', 'Transportation', 'Utilities'];
        return industrylist;
    },

    create_contact: function (payload) {
        // var contact = {email: "abhiany.ketha@gmail.com",firstname: "dfgdshhmhjky",lastname: "req.body.last_name",title: "req.body.job_title",
        //                 AssistantName: "AssistantName",AssistantPhone: "AssistantPhone",Birthdate: "1997-04-26",Department: "Department",
        //                 Description: "Description",MobilePhone: "1234567890",description: "description",leadsource: 'Clearing Microsite'};
        var contact = {
            email: payload.email,
            firstname: payload.firstname,
            lastname: payload.lastname,
            title: payload.title,
            AssistantName: payload.asstname,
            AssistantPhone: payload.asstphone,
            Birthdate: payload.dob,
            Department: payload.department,
            Description: payload.desc,
            MobilePhone: payload.mobile
        };
        return new Promise((resolve, reject) => {
            conn.sobject("Contact").create(contact, (err, ret) => {
                if (err || !ret.success) return reject(err);
                resolve(ret);
            });
        });
    },

    fetch_account_by_id: function (params) {
        return new Promise((resolve, reject) => {
            conn.sobject("Account").retrieve(params.accid, function (err, account) {
                if (err) return reject(err);
                resolve(account);
            });
        });

    },

    fetch_all_accounts: function () {
        return new Promise((resolve, reject) => {
            var records = [];
            var query = conn.query("SELECT Id, Name FROM Account")
                .on("record", function (record) {
                    records.push(record);
                })
                .on("end", function () {
                    resolve(records);
                })
                .on("error", function (err) {
                    // console.error(err);
                    reject(err);
                })
                .run({autoFetch: true, maxFetch: 4000});
        });
    }
}