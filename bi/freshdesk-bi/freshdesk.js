var fd = require('freshdesk-nodejs'),
    appconfig = require('../../config/bi-config'),
    freshdesk = new fd(appconfig.freshdesk.uri, appconfig.freshdesk.apikey);

module.exports = {
    createticket: function (payload) {
        // const newTicket = {
        //     "description": "My printer stopped working", "subject": "Printers are Satan's children",
        //     "email": "abhinay.ketha@gmail.com", "priority": 1, "status": 2
        // };
        const newTicket = {
            "description": payload.desc,
            "subject": paylod.sub,
            "email": payload.email,
            "priority": payload.priority,
            "status": payload.status
        };
        return new Promise(function (resolve, reject) {
            freshdesk.createTicket(newTicket, function (err, res, body) {
                if (err) reject(err)
                else resolve(body)
            });
        });
    },

    ticket_status: function (id) {
        return new Promise(function (resolve, reject) {
            freshdesk.getTicket(id, function (err, res, body) {
                if (err) reject(err)
                else resolve(body)
            })
        });
    },
    create_company: function (payload) {
        // const company = {
        //     "name": "SuperNova","domains": ["supernova", "nova"],"description": "Spaceship Manufacturing Company"
        // };
        const company = {"name": payload.name, "domains": payload.domains, "description": payload.desc};
        return new Promise(function (resolve, reject) {
            freshdesk.createCompany(company, function (err, res, body) {
                if (err) reject(err)
                else resolve(body)
            })
        });
    },

    list_forum_categories: function () {
        return new Promise(function (resolve, reject) {
            freshdesk.listForumCategories(function (err, res, body) {
                if (err) reject(err)
                else resolve(body)
            });
        });
    },

    list_all_forums_in_categories: function (id) {
        return new Promise(function (resolve, reject) {
            freshdesk.listAllForumsInCatogory(id, function (err, res, body) {
                if (err) reject(err);
                else resolve(body);
            })
        });
    },

    create_forum_topic: function (payload) {
        // const topic = { "sticky":true,  "locked":false, "title":"how to create a custom field", "message":"Can someone give me the steps..."};
        const topic = {
            "sticky": payload.sticky, "locked": payload.locked, "title": payload.title, "message": payload.message
        };
        return new Promise(function (resolve, reject) {
            freshdesk.createForumTopic(payload.forumid, topic, function (err, res, body) {
                if (err) reject(err)
                else resolve(body)
            })
        });
    }
}
