var zendesk = require('node-zendesk'),
    appconfig = require('../../config/bi-config'),
    client;
function zendesk_uri_construct(domain) {
    return 'https://'+domain+'/api/v2';
}
module.exports = {
    initialise_module: function (config) {
        // client = zendesk.createClient({
        //     username: appconfig.zendesk1.username,
        //     token: appconfig.zendesk1.apikey,
        //     remoteUri: appconfig.zendesk1.uri
        // })
        client = zendesk.createClient({
            username: config.username,
            token: config.apikey,
            remoteUri: zendesk_uri_construct(config.domain)
        })

    },

    create_ticket: function (payload) {
        // var ticket = {"ticket": {"subject": "pc restarting problem",description:'blue screen errror',assignee_id:8522077489,status:'pending',priority:'urgent',type:'problem',tags:['screen','blue']}};
        var ticket = {
            ticket: {
                subject: payload.subject,
                description: payload.desc,
                assignee_id: payload.assignee,
                status: payload.status,
                priority: paylaod.priority,
                type: payload.type,
                tags: payload.tags
            }
        };
        return new Promise(function (resolve, reject) {
            client.tickets.create(ticket, function (err, req, result) {
                if (err) reject(err)
                else resolve(result)
            });
        });
    },

    create_org: function (payload) {
        // const org = {organization: {name:'testtagdevelopment_era_start',details:'rising_remo',domain_names:['sunera_tech','ebutor_root']}};
        const org = {
            organization: {
                name: payload.name,
                details: payload.details,
                domain_names: payload.domains
            }
        };
        return new Promise(function (resolve, reject) {
            client.organizations.create(org, function (err, res, body) {
                if (err) reject(err);
                else resolve(body)
            });
        });
    },

    create_user: function (payload) {
        // var user = {"user": {"name": "arunkumar","email": "arunkuamr@example.org",phone:'9032336312',details:'coder',role:'agent'}};
        var user = {user: {name: payload.name, email: payload.email, phone: payload.phone, role: payload.phone}};
        return new Promise(function (resolve, reject) {
            client.users.create(user, function (err, req, result) {
                if (err) reject(err);
                else resolve(result);
            });
        });
    },

    list_users: function () {
        return new Promise(function (resolve, reject) {
            client.users.list(function (err, res, data) {
                if (err) reject(err);
                else resolve(data);
            })
        })
    },

    find_user: function (userid) {
        return new Promise(function (resolve, reject) {
            client.users.show(userid, function (err, res, body) {
                if (err) reject(err);
                else resolve(body)
            });
        });
    },

    find_ticket: function (ticketid) {
        return new Promise(function (resolve, reject) {
            client.tickets.show(ticketid, function (err, res, body) {
                if (err) reject(err);
                else resolve(body)
            });
        });
    },
}