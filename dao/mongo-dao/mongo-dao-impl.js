var mongo_instance = require('../../databases/mongo/mongo-db'),
    appconfig = require('../../config/bi-config'),
    request = require('request');

function textsearch_url_construct(search,pincode) {
    return 'https://maps.googleapis.com/maps/api/place/textsearch/json?query='+search+'+in+'+pincode+'&key='+appconfig.google_maps.apikey;
};

function nearbysearch_url_construct(type,search,lat,long,radius) {
    return 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?type='+type+'&keyword='+search+'&location='+lat+','+long+'&radius='+radius+'&key='+appconfig.google_maps.apikey;
};

function geolocation_url_construct(pincode) {
    return 'http://maps.googleapis.com/maps/api/geocode/json?address='+pincode+'&sensor=false';
}
module.exports = {
    insert_dealer_loc: (payload) => {
        return new Promise((resolve, reject) => {
            mongo_instance.dealer_loc.findOneAndUpdate({
                bot_id: payload.bot_id,
                pincode: payload.pincode
            }, payload, {upsert: true, new: true}, (err, res) => {
                if (err) return reject(err);
                resolve(res);
            });
        });
    },

    fetch_dealer: (params) => {
        return new Promise((resolve, reject) => {
            mongo_instance.dealer_loc.find({
                bot_id: params.bot_id,
                pincode: params.pincode
            }, (err, res) => {
                if (err) return reject(err);
                resolve(res);
            });
        });
    },

    googlemaps_textsearch: (params) => {
        return new Promise((resolve,reject) => {
            request(textsearch_url_construct(params.search,params.pincode),(err,res,body) => {
            if(err) return reject(err);
                resolve(body);
            })
        });
    },

    googlemaps_nearbysearch: (params) => {
        return new Promise((resolve,reject) => {
           request(geolocation_url_construct(params.pincode),(err,res,body) => {
                if(err) return reject(err)
               var parsedata = JSON.parse(body).results[0].geometry.location;
               var latlang = parsedata;
               request(nearbysearch_url_construct(params.type,params.search,latlang.lat,latlang.lng,params.radius),(err1,res,body) => {
                    if(err1) return reject(e1rr);
                    resolve(body);
               })
           })
        });
    }
}
